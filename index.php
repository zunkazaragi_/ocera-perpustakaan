<!DOCTYPE html>
<html lang="id">
    <head>
        <?php include "view/components/head.php"; ?>
    </head>

    <body onscroll="scroll()">

        <?php include "view/components/navbar.php"; ?>
        <?php include "view/components/hero-slider.php"; ?>
        <?php include "view/components/top.php";
        ?>
        <?php include
        "view/components/list-book.php";
        ?>
        <?php include "view/components/lets-signup.php";
        ?>
        <?php include "view/components/footer.php";
        ?>
        <?php include "view/components/copyright.php";
        ?>
            

    <script src="assets/js/slideshow.js"></script>
    <script src="assets/js/navbar.js"></script>
    </body>


</html>