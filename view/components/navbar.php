<div class="container " id="navigation">
    <input type="checkbox" id="nav" class="hidden">
    <label for="nav" id="nav-btn">
        <i></i>
        <i></i>
        <i></i>
    </label>

    <div class="logo">
        <img src="assets/logo.svg" alt="">
    </div>
    
    <div class="nav-wrapper">
        <ul>
            <li><a href="#">Login</a></li>
            <li><a href="#">Sign-up</a></li>
        </ul>
    </div>

</div>