<div class="container" id="hero-slider">
    <div class="slider">

        <div class="slide-content fade">
            <img src="assets/img/thrall.jpg" alt="">
            <div class="caption">
                <h2>Thrall</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum impedit eius quia itaque laboriosam dolor. Harum modi a itaque inventore debitis dolore placeat omnis praesentium, fuga, officia eum, sequi maxime.</p>
            </div>
        </div>
        <div class="slide-content fade">
            <img src="assets/img/invoker.jpg" alt="">
            <div class="caption">
                <h2>Invoker</h2>
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Laudantium repudiandae tempora accusantium repellat a perspiciatis voluptate alias consequuntur autem vero amet asperiores, nisi voluptas, animi similique sint commodi aspernatur enim!</p>
            </div>
        </div>
        <div class="slide-content fade">
            <img src="assets/img/lich-king.jpg" alt="">
            <div class="caption">
                <h2>Lich King</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus quos quasi, inventore, sit obcaecati hic officiis non, ipsam numquam iure modi ex sint! Maxime illum minima animi, dignissimos nisi corporis!</p>
            </div>
        </div>

    </div>

    <div class="slide-nav">
        <span class="dot" onclick="currentSlide(1)"></span>
        <span class="dot" onclick="currentSlide(2)"></span>
        <span class="dot" onclick="currentSlide(3)"></span>
    </div>
    
</div>