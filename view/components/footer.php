<div class="container" id="footer">
    <div class="user">
        <h2>Lets Get Read</h2>
        <ul>
            <li>Login</li>
            <li>Signup</li>
        </ul>
    </div>
    <div class="books">
        <h2>Find Interesting Book</h2>
        <ul>
            <li>Genre</li>
            <li>Type</li>
            <li>Top Rated</li>
        </ul>
    </div>
    <div class="about">
        <h2>Ocera Library</h2>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Expedita in esse eveniet neque numquam tenetur facilis ipsam facere sequi illum autem tempore magnam sint eos atque sed, natus dolore veniam?</p>
        <div class="search">
            <input type="text" name="search">
            <button>Search</button>
        </div>
    </div>
</div>